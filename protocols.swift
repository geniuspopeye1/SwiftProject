//First
struct IOSCollection<Element>: Collection {
    
    private var buffer: Buffer
    
    init<T>(_ element: T) {
        self.buffer = Buffer(elements: element)
    }
    
    mutating func append(_ element: T) {
        if !isKnownUniquelyReferenced(&buffer) {
            buffer = buffer.copy()
        }
        buffer.append(element)
    }
    
    private class Buffer {
        
        private var element: T
        
        init(element: T) {
            self.element = element
        }
        
        func copy() -> Buffer {
            return Buffer(element: element)
        }
        
        func append(_ element: Element) {
            element.append(element)
        }
    }
}

//Second
protocol Hotel {
    var roomCount: Int { get }
    
    init(roomCount: Int)
}

class HotelAlfa: Hotel {
    var roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}

//Third
protocol GameDice {
    var numberDice: Int { get }
}

extension Int: GameDice {
    var numberDice: Int {
        print("Выпало \(self) на кубике")
        return self
    }
}

//Fourth
protocol MyProtocol {
    var requiredProperty: String { get }
    var optionalProperty: Int? { get }
    
    func myMethod()
}

class MyClass: MyProtocol {
    var requiredProperty: String
    
    init(requiredProperty: String) {
        self.requiredProperty = requiredProperty
    }
    
    func myMethod() {
        print("Выполняем метод MyProtocol")
    }
}

//Fifth 
protocol Coding {
    var time: Double { get }
    var codeCount: Int { get }
    
    func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol CodingControl {
    func stopCoding()
}

class Company: Coding, CodingControl {
    var numberOfProgrammers: Int
    var specializations: [Platform]
    
    var time: Double = 0.0
    var codeCount: Int = 0
    
    init(numberOfProgrammers: Int, specializations: [Platform]) {
        self.numberOfProgrammers = numberOfProgrammers
        self.specializations = specializations
    }
    
    func writeCode(platform: Platform, numberOfSpecialist: Int) {
        print("Написание кода на платформе \(platform.rawValue) с помощью \(numberOfSpecialist) специалистов")
        self.codeCount += numberOfSpecialist * 100
    }
    
    func stopCoding() {
        print("Остановка написания кода")
    }
    
    func startCoding(projectName: String) {
        print("Разработка началась. Пишем код для проекта \(projectName)")
    }
    
    func endCoding() {
        print("Работа закончена. Сдаю в тестирование.")
    }
}

enum Platform: String {
    case ios = "iOS"
    case android = "Android"
    case web = "Web"
}
