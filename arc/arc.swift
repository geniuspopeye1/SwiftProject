// Определяем структуру автомобиля
struct Car {
    let make: String // марка автомобиля
    let year: Int // год выпуска
    var trunkVolume: Double // объем багажника
    var isEngineOn: Bool // запущен ли двигатель
    var areWindowsOpen: Bool // открыты ли окна
    var filledTrunkVolume: Double // заполненный объем багажника
    
    // Метод, который позволяет загрузить груз в багажник
    mutating func loadCargo(volume: Double) {
        if filledTrunkVolume + volume > trunkVolume {
            print("Не могу загрузить груз. Багажник полный.")
        } else {
            filledTrunkVolume += volume
            print("Загрузка выполнена успешно.")
        }
    }
    
    // Метод, который позволяет выгрузить груз из багажника
    mutating func unloadCargo(volume: Double) {
        if filledTrunkVolume - volume < 0 {
            print("Не могу выгрузить груз. Багажник пуст.")
        } else {
            filledTrunkVolume -= volume
            print("Выгрузка выполнена успешно.")
        }
    }
    
    // Метод, который позволяет запустить двигатель
    mutating func turnOnEngine() {
        if isEngineOn {
            print("Двигатель уже запущен.")
        } else {
            isEngineOn = true
            print("Двигатель запущен.")
        }
    }
    
    // Метод, который позволяет заглушить двигатель
    mutating func turnOffEngine() {
        if !isEngineOn {
            print("Двигатель уже заглушен.")
        } else {
            isEngineOn = false
            print("Двигатель заглушен.")
        }
    }
    
    // Метод, который позволяет открыть окна
    mutating func openWindows() {
        if areWindowsOpen {
            print("Окна уже открыты.")
        } else {
            areWindowsOpen = true
            print("Окна открыты.")
        }
    }
    
    // Метод, который позволяет закрыть окна
    mutating func closeWindows() {
        if !areWindowsOpen {
            print("Окна уже закрыты.")
        } else {
            areWindowsOpen = false
            print("Окна закрыты.")
        }
    }

    
    enum CarAction {
        case turnEngineOn
        case turnEngineOff
        case openWindows
        case closeWindows
        case loadTrunk(volume: Int)
        case unloadTrunk(volume: Int)
    }
    
    mutating func performAction(_ action: CarAction) {
        switch action {
        case .turnEngineOn:
            isEngineOn = true
        case .turnEngineOff:
            isEngineOn = false
        case .openWindows:
            areWindowsOpen = true
        case .closeWindows:
            areWindowsOpen = false
        case .loadTrunk(let volume):
            filledTrunkVolume += volume
        case .unloadTrunk(let volume):
            filledTrunkVolume -= volume
            if filledTrunkVolume < 0 {
                filledTrunkVolume = 0
            }
        }
    }
}

// Структура грузовика
struct Truck {
    let make: String
    let year: Int
    let trunkVolume: Double
    var isEngineOn: Bool
    var areWindowsOpen: Bool
    var filledTrunkVolume: Double
    let cargoVolume: Double
    
    // Метод, который позволяет загрузить груз в кузов грузовика
    mutating func loadCargo(volume: Double) {
        if filledTrunkVolume + volume > trunkVolume {
            print("Не могу загрузить груз. Кузов грузовика полный.")
        } else if volume > cargoVolume {
            print("Не могу загрузить груз. Объем груза больше, чем доступное место в кузове.")
        } else {
            filledTrunkVolume += volume
            print("Загрузка выполнена успешно.")
        }
    }
    
    // Метод, который позволяет выгрузить груз из кузова грузовика
    mutating func unloadCargo(volume: Double) {
        if filledTrunkVolume - volume < 0 {
            print("Не могу выгрузить груз. Кузов грузовика пуст.")
        } else {
            filledTrunkVolume -= volume
            print("Выгрузка выполнена успешно.")
        }
    }

    //Метод для изменения свойств структуры
    enum TruckAction {
        case turnEngineOn
        case turnEngineOff
        case openWindows
        case closeWindows
        case loadCargo(volume: Int)
        case unloadCargo(volume: Int)
    }
    
    mutating func performAction(_ action: TruckAction) {
        switch action {
        case .turnEngineOn:
            isEngineOn = true
        case .turnEngineOff:
            isEngineOn = false
        case .openWindows:
            areWindowsOpen = true
        case .closeWindows:
            areWindowsOpen = false
        case .loadCargo(let volume):
            filledCargoVolume += volume
        case .unloadCargo(let volume):
            filledCargoVolume -= volume
            if filledCargoVolume < 0 {
                filledCargoVolume = 0
            }
        }
    }
}

// Создаем экземпляры структур Car и Truck
var car = Car(make: "Toyota", year: 2021, trunkVolume: 500, isEngineOn: false, areWindowsOpen: false, filledTrunkVolume: 0)
var truck = Truck(make: "Volvo", year: 2022, trunkVolume: 2000, isEngineOn: false, areWindowsOpen: false, filledTrunkVolume: 0, cargoVolume: 2000)

// Используем методы, чтобы выполнить действия с автомобилями
car.turnOnEngine()
car.turnOnEngine()
car.loadCargo(volume: 300)
car.loadCargo(volume: 300)
car.loadCargo(volume: 300) 
car.unloadCargo(volume: 100)
car.closeWindows()

truck.turnOnEngine() 
truck.loadCargo(volume: 1500) 
truck.loadCargo(volume: 1500) 
truck.unloadCargo(volume: 1000) 
truck.closeWindows() 

// Словарь
var dict: [AnyHashable: String] = [Car(make: "Toyota", year: 2021, trunkVolume: 500, isEngineOn: false, areWindowsOpen: false, filledTrunkVolume: 0): "structCar", Truck(make: "Volvo", year: 2022, trunkVolume: 2000, isEngineOn: false, areWindowsOpen: false, filledTrunkVolume: 0, cargoVolume: 2000): "structTruck"]






