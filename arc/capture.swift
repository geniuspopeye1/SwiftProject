//Capture List - это механизм, который позволяет захватывать значения извне, для использования внутри замыкания. 

var sortOrder = -1
let numbers = [5, 3, 9, 7, 1]
let sortedNumbers = numbers.sorted { [sortOrder] a, b in
    return sortOrder * a < sortOrder * b
}

//Сортировка в обратном порядке
//Замыкание используется для сравнения элементов массива numbers. 
//Capture List используется, чтобы захватить переменную sortOrder извне замыкания и использовать ее для определения порядка сортировки. 
//Замыкание передается в метод sorted(by:) и сохраняем отсортированный массив в переменной sortedNumbers.