var myArray = [5, 3, 8, 4, 1, 2, 9, 7, 6]
myArray.sort(by: { $0 < $1 })
print("Отсортированный массив: \(myArray)")

myArray.sort(by: { $0 > $1 })
print("Отсортированный массив в обратном порядке: \(myArray)")

func sortFriendsByNameLength(_ friends: [String]) -> [String] {
    return friends.sorted(by: { $0.count < $1.count })
}

var friends = ["Дмитрий", "Иван", "Мария", "Анна", "Рашид"]
friends = sortFriendsByNameLength(friends)
print("Отсортированный массив друзей по количеству букв в имени: \(friends)")

var friendsDictionary = [Int: String]()
for friend in friends {
    friendsDictionary[friend.count] = friend
}

func printFriend(count: Int, name: String) {
    print("У друга \(name) \(count) букв в имени")
}

func checkArrays(_ array1: inout [String], _ array2: inout [Int]) {
    if array1.isEmpty {
        array1.append("Один")
    }
    
    if array2.isEmpty {
        array2.append(1)
    }
    
    print("Массив строк: \(array1)")
    print("Массив чисел: \(array2)")
}

// Пример использования всех функций
printFriend(count: friends[0].count, name: friends[0])
checkArrays(["Два", "Три"], [])
