let daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

let monthNames = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]

print("Количество дней в каждом месяце:")
for days in daysInMonths {
    print(days)
}

var monthData = [(String, Int)]()

for i in 0..<daysInMonths.count {
    monthData.append((monthNames[i], daysInMonths[i]))
}

print("\nНазвание месяца и количество дней:")
for data in monthData {
    print("\(data.0): \(data.1)")
}

print("\nНазвание месяца и количество дней (используя массив tuples):")
for (month, days) in zip(monthNames, daysInMonths) {
    print("\(month): \(days)")
}

print("\nКоличество дней в каждом месяце в обратном порядке:")
for days in daysInMonths.reversed() {
    print(days)
}

let month = 3
let day = 15

var daysToDate = 0
for i in 0..<month {
    daysToDate += daysInMonths[i]
}
daysToDate += day - 1

print("\nКоличество дней до \(day).\(month + 1): \(daysToDate)")