enum FirstEnum: Int {
    case first = 1
    case second
}

enum SecondEnum: String {
    case first = "First"
    case second = "Second"
}

enum Gender {
    case male
    case female
}

enum Age {
    case child
    case teenager
    case adult
    case senior
}

enum Experience {
    case beginner
    case intermediate
    case advanced
}

enum RainbowColor: String {
    case red
    case orange
    case yellow
    case green
    case blue
    case indigo
    case violet
}

func printEnumCases() {
    let firstEnumCases = [FirstEnum.first, SecondEnum.second]
    let rainbowColors = [RainbowColor.red, RainbowColor.orange, RainbowColor.yellow, RainbowColor.green, RainbowColor.blue, RainbowColor.indigo, RainbowColor.violet]
    
    for color in rainbowColors {
        print("\(color.rawValue) light")
    }
}

enum Score: String {
    case A = "Отлично"
    case B = "Хорошо"
    case C = "Удовлетворительно"
    case D = "Неудовлестворительно"
    case F = "Отчислен"
}

func printScoreValue(score: Score) {
    switch score {
    case .A:
        print("5")
    case .B:
        print("4")
    case .C:
        print("3")
    case .D:
        print("2")
    case .F:
        print("1")
    }
}

enum Car: String {
    case ford = "Ford"
    case toyota = "Toyota"
    case honda = "Honda"
    case bmw = "BMW"
}

struct Garage {
    var cars: [Car]
    
    func printCars() {
        for car in cars {
            print("\(car.rawValue) is in the garage")
        }
    }
}

let myGarage = Garage(cars: [.ford, .bmw])
myGarage.printCars()
