// Создание класса родитель и 2 классов наследника
class ParentClass {

}

class ChildClass1: ParentClass {
  
}

class ChildClass2: ParentClass {
  
}

// Создание класса House с несколькими свойствами и методами
class House {
  var width: Int
  var height: Int
  
  init(width: Int, height: Int) {
    self.width = width
    self.height = height
  }
  
  func create() {
    let area = width * height
    print("Площадь дома: \(area)")
  }
  
  func destroy() {
    print("Дом уничтожен!")
  }
}

// Создание класса с методами, которые сортируют массив учеников по разным параметрам
class StudentSorter {
  func sortByAge(_ students: [Student]) -> [Student] {
    return students.sorted(by: { $0.age < $1.age })
  }
  
  func sortByName(_ students: [Student]) -> [Student] {
    return students.sorted(by: { $0.name < $1.name })
  }
}

// Структура для хранения координат x и y
struct Point {
    var x: Double
    var y: Double
}

// Класс для хранения информации о прямоугольнике
class Rectangle {
    var width: Double
    var height: Double
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
    
    func area() -> Double {
        return width * height
    }
}
/*
  Отличия между структурами и классами:
  - Структуры - это значения, классы - это ссылки на значения
  - Структуры передаются по значению, классы передаются по ссылке
  - Структуры не могут наследовать друг от друга, классы могут
*/

